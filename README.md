## Front-End Developer Experience ##

I chose this project to experience building from a design mock-up. You can find this photoshop template and many other design templates at [BitByChip][1]. I decided to use bootstrap for this project, so that I could utilize the responsive grid system and the javascript dropdown menu component. The photoshop template did not include responsive design mock-ups, so I adapted the design to become responsive. The most time consuming part of this project was creating the "articles" section with the four images and overwriting bootstrap for the header navigation. In addition, I was not satisfied with bootstrap's breakpoints, so I also created a minor breakpoint at 500px. You can view this project at [rlmoser.com][2].

[1]: http://bitbychip.com/100-free-psd-photoshop-website-templates/
[2]: https://rlmoser.com/work.html
