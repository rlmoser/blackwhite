'use strict';
module.exports = function(grunt) {
  require('load-grunt-tasks')(grunt);

  var site = {
      source: 'src',
      dest: 'dist',
      temp: '.tmp',
    };

  grunt.initConfig({

    jshint: {
      options: {
        jshintrc: '.jshintrc',
        reporter: require('jshint-stylish')
      },
      all: site.source + '/**/*.js',
    },

    csslint: {
      options: { csslintrc: '.csslintrc' },
      check: {
        src: site.source + '/**/*.css',
      }
    },

    watch: {
      options: {
        interrupt: true,
        livereload: true,
        livereloadOnError: false
      },
      js: {
        files: site.source + '/**/*.js',
        tasks: ['jshint:all']
      },
      css: {
        files: site.source + '/**/*.css',
        tasks: ['csslint:check']
      },
      html: {
        files: site.source + '/**/*.html',
      },
      configFiles: {
        files: [ 'Gruntfile.js' ],
        options: { reload: true }
      }
    },

    copy: {
      fonts: {
        files: [{
          // bring in vendor font files
          expand: true,
          flatten: true,
          dot: true,
          cwd: 'bower_components/',
          src: ['font-awesome/fonts/*', 'bootstrap/fonts/*'],
          dest: site.source + '/fonts'
        }]
      }
    },
    connect: {
      server: {
        options: {
          base: ['bower_components', site.source],
          directory: site.source,
          hostname: '*',
          livereload: true,
          open: {
            appName: 'open',
            target: 'http://localhost:3000',
          },
          port: 3000,
        }
      }
    }
  });

  // Tasks
  grunt.registerTask('serve', function () {
    grunt.task.run([
      'jshint:all',
      'csslint:check',
      'copy',
      'connect',
      'watch'
    ]);
  });

  grunt.registerTask('default', ['serve']);
};
